'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const when = require('when');
const client = require('./client');

const follow = require('./follow'); 

const stompClient = require('./websocket-listener');

const root = '/api';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {agendas: [], attributes: [], page: 1, pageSize: 2, links: {}};
		this.updatePageSize = this.updatePageSize.bind(this);
		this.onCreate = this.onCreate.bind(this);
		this.onUpdate = this.onUpdate.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
		this.refreshCurrentPage = this.refreshCurrentPage.bind(this);
		this.refreshAndGoToLastPage = this.refreshAndGoToLastPage.bind(this);
	}

	loadFromServer(pageSize) {
		follow(client, root, [
				{rel: 'agendas', params: {size: pageSize}}]
		).then(agendaCollection => {
				return client({
					method: 'GET',
					path: agendaCollection.entity._links.profile.href,
					headers: {'Accept': 'application/schema+json'}
				}).then(schema => {
					this.schema = schema.entity;
					this.links = agendaCollection.entity._links;
					return agendaCollection;
				});
		}).then(agendaCollection => {
			this.page = agendaCollection.entity.page;
			return agendaCollection.entity._embedded.agendas.map(agenda =>
					client({
						method: 'GET',
						path: agenda._links.self.href
					})
			);
		}).then(agendaPromises => {
			return when.all(agendaPromises);
		}).done(agendas => {
			this.setState({
				page: this.page,
				agendas: agendas,
				attributes: Object.keys(this.schema.properties),
				pageSize: pageSize,
				links: this.links
			});
		});
	}

	// tag::on-create[]
	onCreate(newAgenda) {
		follow(client, root, ['agendas']).done(response => {
			client({
				method: 'POST',
				path: response.entity._links.self.href,
				entity: newAgenda,
				headers: {'Content-Type': 'application/json'}
			})
		})
	}
	// end::on-create[]

	onUpdate(agenda, updatedAgenda) {
		client({
			method: 'PUT',
			path: agenda.entity._links.self.href,
			entity: updatedAgenda,
			headers: {
				'Content-Type': 'application/json',
				'If-Match': agenda.headers.Etag
			}
		}).done(response => {
			/* Let the websocket handler update the state */
		}, response => {
			if (response.status.code === 412) {
				alert('DENIED: Unable to update ' + agenda.entity._links.self.href + '. Your copy is stale.');
			}
		});
	}

	onDelete(agenda) {
		client({method: 'DELETE', path: agenda.entity._links.self.href});
	}

	onNavigate(navUri) {
		client({
			method: 'GET',
			path: navUri
		}).then(agendaCollection => {
			this.links = agendaCollection.entity._links;
			this.page = agendaCollection.entity.page;

			return agendaCollection.entity._embedded.agendas.map(agenda =>
					client({
						method: 'GET',
						path: agenda._links.self.href
					})
			);
		}).then(agendaPromises => {
			return when.all(agendaPromises);
		}).done(agendas => {
			this.setState({
				page: this.page,
				agendas: agendas,
				attributes: Object.keys(this.schema.properties),
				pageSize: this.state.pageSize,
				links: this.links
			});
		});
	}

	updatePageSize(pageSize) {
		if (pageSize !== this.state.pageSize) {
			this.loadFromServer(pageSize);
		}
	}

	// tag::websocket-handlers[]
	refreshAndGoToLastPage(message) {
		follow(client, root, [{
			rel: 'agendas',
			params: {size: this.state.pageSize}
		}]).done(response => {
			if (response.entity._links.last !== undefined) {
				this.onNavigate(response.entity._links.last.href);
			} else {
				this.onNavigate(response.entity._links.self.href);
			}
		})
	}

	refreshCurrentPage(message) {
		follow(client, root, [{
			rel: 'agendas',
			params: {
				size: this.state.pageSize,
				page: this.state.page.number
			}
		}]).then(agendaCollection => {
			this.links = agendaCollection.entity._links;
			this.page = agendaCollection.entity.page;

			return agendaCollection.entity._embedded.agendas.map(agenda => {
				return client({
					method: 'GET',
					path: agenda._links.self.href
				})
			});
		}).then(agendaPromises => {
			return when.all(agendaPromises);
		}).then(agendas => {
			this.setState({
				page: this.page,
				agendas: agendas,
				attributes: Object.keys(this.schema.properties),
				pageSize: this.state.pageSize,
				links: this.links
			});
		});
	}
	// end::websocket-handlers[]

	// tag::register-handlers[]
	componentDidMount() {
		this.loadFromServer(this.state.pageSize);
		stompClient.register([
			{route: '/topic/newAgenda', callback: this.refreshAndGoToLastPage},
			{route: '/topic/updateAgenda', callback: this.refreshCurrentPage},
			{route: '/topic/deleteAgenda', callback: this.refreshCurrentPage}
		]);
	}
	// end::register-handlers[]

	render() {
		return (
			<div>
				<CreateDialog attributes={this.state.attributes} onCreate={this.onCreate}/>
				<AgendaList page={this.state.page}
							  agendas={this.state.agendas}
							  links={this.state.links}
							  pageSize={this.state.pageSize}
							  attributes={this.state.attributes}
							  onNavigate={this.onNavigate}
							  onUpdate={this.onUpdate}
							  onDelete={this.onDelete}
							  updatePageSize={this.updatePageSize}/>
			</div>
		)
	}
}

class CreateDialog extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		const newAgenda = {};
		this.props.attributes.forEach(attribute => {
			newAgenda[attribute] = ReactDOM.findDOMNode(this.refs[attribute]).value.trim();
		});
		this.props.onCreate(newAgenda);
		this.props.attributes.forEach(attribute => {
			ReactDOM.findDOMNode(this.refs[attribute]).value = ''; // clear out the dialog's inputs
		});
		window.location = "#";
	}

	render() {
		const inputs = this.props.attributes.map(attribute =>
			<p key={attribute}>
				<input type="text" placeholder={attribute} ref={attribute} className="field"/>
			</p>
		);
		return (
			<div>
				<a href="#createAgenda">Create</a>

				<div id="createAgenda" className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Criar novo Evento na Agenda</h2>

						<form>
							{inputs}
							<button onClick={this.handleSubmit}>Create</button>
						</form>
					</div>
				</div>
			</div>
		)
	}
}

class UpdateDialog extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		const updatedAgenda = {};
		this.props.attributes.forEach(attribute => {
			updatedAgenda[attribute] = ReactDOM.findDOMNode(this.refs[attribute]).value.trim();
		});
		this.props.onUpdate(this.props.agenda, updatedAgenda);
		window.location = "#";
	}

	render() {
		const inputs = this.props.attributes.map(attribute =>
			<p key={this.props.agenda.entity[attribute]}>
				<input type="text" placeholder={attribute}
					   defaultValue={this.props.agenda.entity[attribute]}
					   ref={attribute} className="field"/>
			</p>
		);

		const dialogId = "updateAgenda-" + this.props.agenda.entity._links.self.href;

		return (
			<div>
				<a href={"#" + dialogId}>Update</a>

				<div id={dialogId} className="modalDialog">
					<div>
						<a href="#" title="Close" className="close">X</a>

						<h2>Editar o Evento Agenda</h2>

						<form>
							{inputs}
							<button onClick={this.handleSubmit}>Update</button>
						</form>
					</div>
				</div>
			</div>
		)
	}

}

class AgendaList extends React.Component {

	constructor(props) {
		super(props);
		this.handleNavFirst = this.handleNavFirst.bind(this);
		this.handleNavPrev = this.handleNavPrev.bind(this);
		this.handleNavNext = this.handleNavNext.bind(this);
		this.handleNavLast = this.handleNavLast.bind(this);
		this.handleInput = this.handleInput.bind(this);
	}

	handleInput(e) {
		e.preventDefault();
		const pageSize = ReactDOM.findDOMNode(this.refs.pageSize).value;
		if (/^[0-9]+$/.test(pageSize)) {
			this.props.updatePageSize(pageSize);
		} else {
			ReactDOM.findDOMNode(this.refs.pageSize).value = pageSize.substring(0, pageSize.length - 1);
		}
	}

	handleNavFirst(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.first.href);
	}

	handleNavPrev(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.prev.href);
	}

	handleNavNext(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.next.href);
	}

	handleNavLast(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.last.href);
	}

	render() {
		const pageInfo = this.props.page.hasOwnProperty("number") ?
			<h3>Eventos - Page {this.props.page.number + 1} of {this.props.page.totalPages}</h3> : null;

		const agendas = this.props.agendas.map(agenda =>
			<Agenda key={agenda.entity._links.self.href}
					  agenda={agenda}
					  attributes={this.props.attributes}
					  onUpdate={this.props.onUpdate}
					  onDelete={this.props.onDelete}/>
		);

		const navLinks = [];
		if ("first" in this.props.links) {
			navLinks.push(<button key="first" onClick={this.handleNavFirst}>&lt;&lt;</button>);
		}
		if ("prev" in this.props.links) {
			navLinks.push(<button key="prev" onClick={this.handleNavPrev}>&lt;</button>);
		}
		if ("next" in this.props.links) {
			navLinks.push(<button key="next" onClick={this.handleNavNext}>&gt;</button>);
		}
		if ("last" in this.props.links) {
			navLinks.push(<button key="last" onClick={this.handleNavLast}>&gt;&gt;</button>);
		}

		return (
			<div>
				{pageInfo}
				<input ref="pageSize" defaultValue={this.props.pageSize} onInput={this.handleInput}/>
				<table>
					<tbody>
						<tr>
							<th>Data Inicio</th>
							<th>Data Fim</th>
							<th>Descrição</th>
							<th>Nome</th>
							<th></th>
							<th></th>
						</tr>
						{agendas}
					</tbody>
				</table>
				<div>
					{navLinks}
				</div>
			</div>
		)
	}
}

class Agenda extends React.Component {

	constructor(props) {
		super(props);
		this.handleDelete = this.handleDelete.bind(this);
	}

	handleDelete() {
		this.props.onDelete(this.props.agenda);
	}

	render() {
		return (
			<tr>
			
				<td>{this.props.agenda.entity.dataInicio}</td>
				<td>{this.props.agenda.entity.dataFim}</td>
				<td>{this.props.agenda.entity.descricao}</td>
				<td>{this.props.agenda.entity.nome}</td>
				<td>
					<UpdateDialog agenda={this.props.agenda}
								  attributes={this.props.attributes}
								  onUpdate={this.props.onUpdate}/>
				</td>
				<td>
					<button onClick={this.handleDelete}>Delete</button>
				</td>
			</tr>
		)
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
)
