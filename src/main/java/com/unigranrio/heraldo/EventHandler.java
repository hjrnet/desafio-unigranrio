package  com.unigranrio.heraldo;

import static  com.unigranrio.heraldo.WebSocketConfiguration.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.hateoas.EntityLinks;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Herlado Junior
 */
// tag::code[]
@Component
@RepositoryEventHandler(Agenda.class)
public class EventHandler {

	private final SimpMessagingTemplate websocket;

	private final EntityLinks entityLinks;

	@Autowired
	public EventHandler(SimpMessagingTemplate websocket, EntityLinks entityLinks) {
		this.websocket = websocket;
		this.entityLinks = entityLinks;
	}

	@HandleAfterCreate
	public void newAgenda(Agenda agenda) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/newAgenda", getPath(agenda));
	}

	@HandleAfterDelete
	public void deleteAgenda(Agenda agenda) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/deleteAgenda", getPath(agenda));
	}

	@HandleAfterSave
	public void updateAgenda(Agenda agenda) {
		this.websocket.convertAndSend(
				MESSAGE_PREFIX + "/updateAgenda", getPath(agenda));
	}

	/**
	 * Take an {@link Agenda} and get the URI using Spring Data REST's {@link EntityLinks}.
	 *
	 * @param agenda
	 */
	private String getPath(Agenda agenda) {
		return this.entityLinks.linkForSingleResource(agenda.getClass(),
				agenda.getId()).toUri().getPath();
	}

}
// end::code[]
