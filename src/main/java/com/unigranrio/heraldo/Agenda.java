package  com.unigranrio.heraldo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
/**
 * @author Herlado Junior
 */
// tag::code[]
@Data
@Entity
public class Agenda {

	private @Id @GeneratedValue Long id;
	
	private LocalDateTime  dataInicio;
	private LocalDateTime  dataFim;
	private String descricao;
	private String nome;

	private @Version @JsonIgnore Long version;

	private Agenda() {}

	public Agenda(LocalDateTime dataInicio, LocalDateTime dataFim, String descricao, String nome) {
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.descricao = descricao;
		this.nome = nome;
	}
}
// end::code[]