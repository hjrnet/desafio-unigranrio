package com.unigranrio.heraldo;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author Herlado Junior
 */
// tag::code[]
public interface AgendaRepository extends PagingAndSortingRepository<Agenda, Long> {

}
// end::code[]
