package com.unigranrio.heraldo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
/**
 * @author Herlado Junior
 */
// tag::code[]
@Component
public class DatabaseLoader implements CommandLineRunner {

	private final AgendaRepository repository;

	@Autowired
	public DatabaseLoader(AgendaRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception {
		LocalDateTime now = LocalDateTime.now();
	
		this.repository.save(new Agenda(now,now, "levar o carro na oficina","Oficina"));
		this.repository.save(new Agenda(now,now, "ir ao dentista","Dentista"));
		this.repository.save(new Agenda(now,now, "visitar Karol","Karol"));
		this.repository.save(new Agenda(now,now, "encontro o amigo Dalton","Dalton"));
		this.repository.save(new Agenda(now,now, "Vender o carro para Saulo","Saulo"));
	}
}
// end::code[]