# Desafio UNIGRANRIO

Projeto MVC Back End contruido em Java 8, Maven Project, Spring Boot e Spring Data Rest.
Projeto MVC Front End contruido em React.js coded in ES6.

Dependecias backend.
http://start.spring.io
Rest Repositories JPA, Lombok, Thymeleaf, H2

Dependecias frontend.
react.js - toolkit 

rest.js - CujoJS toolkit used to make REST calls

webpack - toolkit used to compile JavaScript components into a single, loadable bundle

babel - write your JavaScript code using ES6 and compile it into ES5 to run in the browser

Comando para rodar o projeto.
./mvnw spring-boot:run

visitar http://localhost:8080.

